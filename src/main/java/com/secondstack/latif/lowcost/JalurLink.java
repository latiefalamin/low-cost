package com.secondstack.latif.lowcost;

/**
 *
 * @author LATIEF-NEW
 */
public class JalurLink {
    private Link link;
    private JalurBulatan before;
    private JalurBulatan after;

    public Link getLink() {
        return link;
    }

    public JalurBulatan getBefore() {
        return before;
    }

    public void setBefore(JalurBulatan before) {
        this.before = before;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public JalurBulatan getAfter() {
        return after;
    }

    public void setAfter(JalurBulatan after) {
        this.after = after;
    }
}
