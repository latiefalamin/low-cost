package com.secondstack.latif.lowcost;

/**
 *
 * @author LATIEF-NEW
 */
public class JalurBulatan {

    private Bulatan bulatan;
    private JalurLink before;
    private JalurLink after;

    public Bulatan getBulatan() {
        return bulatan;
    }

    public void setBulatan(Bulatan bulatan) {
        this.bulatan = bulatan;
    }

    public JalurLink getBefore() {
        return before;
    }

    public void setBefore(JalurLink before) {
        this.before = before;
    }

    public JalurLink getAfter() {
        return after;
    }

    public void setAfter(JalurLink after) {
        this.after = after;
    }
}
