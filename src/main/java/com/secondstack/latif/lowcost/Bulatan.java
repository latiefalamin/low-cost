package com.secondstack.latif.lowcost;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LATIEF-NEW
 */
public class Bulatan {
    
    private String name;
    private List<Link> toLinks;

    public Bulatan() {
        this.toLinks = new ArrayList<Link>();
    }

    public Bulatan(String name) {
        this.name = name;
        this.toLinks = new ArrayList<Link>();
    }

    public void addLink(Link link){
        toLinks.add(link);
    }

    public List<Link> getToLinks() {
        return toLinks;
    }

    public void setToLinks(List<Link> toLinks) {
        this.toLinks = toLinks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
