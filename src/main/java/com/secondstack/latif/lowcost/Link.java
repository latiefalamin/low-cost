package com.secondstack.latif.lowcost;

/**
 *
 * @author LATIEF-NEW
 */
public class Link {
    
    private String name;
    private int cost;
    private Bulatan toBulatan;

    public Link() {
        cost = 0;
    }

    public Link(String name) {
        this.name = name;
        cost = 0;
    }

    public Link(String name, int cost, Bulatan toBulatan) {
        this.name = name;
        this.cost = cost;
        this.toBulatan = toBulatan;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Bulatan getToBulatan() {
        return toBulatan;
    }

    public void setToBulatan(Bulatan toBulatan) {
        this.toBulatan = toBulatan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
