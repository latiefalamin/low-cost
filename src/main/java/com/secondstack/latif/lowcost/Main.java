package com.secondstack.latif.lowcost;

/**
 * Hello world!
 *
 */
public class Main 
{
    public static void main( String[] args )
    {
        /**
         * Gambaran case bisa dicek di case.docx atau case.pdf pada repo ini.
         */
        
        case1();
        System.out.println("");
        case2();
    }
    
    public static void case2(){
        Bulatan start = new Bulatan("s");
        Bulatan b1 = new Bulatan("b1");
        Bulatan b2 = new Bulatan("b2");
        Bulatan b3 = new Bulatan("b1");
        Bulatan b4 = new Bulatan("b2");
        Bulatan finish = new Bulatan("f");
        
        Link l1 = new Link("L1", 2, b1);
        Link l2 = new Link("L2", 3, b2);
        Link l3 = new Link("L3", 7, b3);
        Link l4 = new Link("L4", 3, b4);
        Link l5 = new Link("L5", 4, b4);
        Link l6 = new Link("L6", 6, finish);
        Link l7 = new Link("L7", 5, finish);
        Link l8 = new Link("L8", 1, finish);
        
        start.addLink(l1);
        start.addLink(l2);
        b1.addLink(l3);
        b1.addLink(l4);
        b2.addLink(l5);
        b2.addLink(l8);
        b3.addLink(l6);
        b4.addLink(l7);
        
        CalculateLowCost clc = new CalculateLowCost();
        clc.setStartBulatan(start);
        clc.setFinishBulatan(finish);
        
        int lowestCost = clc.calculate();
        System.out.println("yang paling murah : " + lowestCost);
    }
    
    public static void case1(){
        Bulatan start = new Bulatan("s");
        Bulatan b1 = new Bulatan("b1");
        Bulatan b2 = new Bulatan("b2");
        Bulatan finish = new Bulatan("f");
        
        Link startLink1 = new Link("s1");
        Link startLink2 = new Link("s2");
        Link finishLink1 = new Link("f1");
        Link finishLink2 = new Link("f2");
        
        startLink1.setToBulatan(b1);
        startLink1.setCost(2);
        
        startLink2.setToBulatan(b2);
        startLink2.setCost(3);
        
        start.addLink(startLink1);
        start.addLink(startLink2);
        
        finishLink1.setCost(2);
        finishLink1.setToBulatan(finish);
        b1.addLink(finishLink1);
        
        finishLink2.setCost(2);
        finishLink2.setToBulatan(finish);
        b2.addLink(finishLink2);
        
        CalculateLowCost clc = new CalculateLowCost();
        clc.setStartBulatan(start);
        clc.setFinishBulatan(finish);
        
        int lowestCost = clc.calculate();
        System.out.println("yang paling murah : " + lowestCost);
    }
}
