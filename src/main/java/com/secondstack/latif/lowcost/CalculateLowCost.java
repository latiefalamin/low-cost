package com.secondstack.latif.lowcost;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LATIEF-NEW
 */
public class CalculateLowCost {

    private Bulatan startBulatan;
    private Bulatan finishBulatan;
    private List<JalurBulatan> jalurs;
    private JalurBulatan lowCostJalurBulatan;

    /**
     * Hitung dan dapatkan cost terendah
     *
     * @return
     */
    public int calculate() {
        if (startBulatan == null || finishBulatan == null) {
            return 0;
        }
        
        //array untuk menyimpan jalur yang start-finish
        this.jalurs = new ArrayList<JalurBulatan>();
        
        //buat dulu jalur start nya
        JalurBulatan startJalurBulatan = new JalurBulatan();
        startJalurBulatan.setBulatan(startBulatan);

        //mulai menghitung semua kemungkinan jalur start - finish
        calculate(startJalurBulatan);

        //dari semua jalur yang sudah ditemukan, dicari yang paling murah
        return calculateMinTotalCost();
    }

    /**
     * Dapatkan minimal total cost
     * @return 
     */
    public int calculateMinTotalCost() {
        if (this.jalurs == null || this.jalurs.isEmpty()) {
            return 0;
        }

        int min = calculateTotalCostByBefore(this.jalurs.get(0));
        System.out.println(" = " + min);
        for (int i = 1; i < this.jalurs.size(); i++) {
            int cost = calculateTotalCostByBefore(this.jalurs.get(i));
            System.out.println(" = " + cost);
            if (cost < min) {
                min = cost;
            }
        }

        return min;
    }

    /**
     * Hitung total biaya jalur dengan asumsi hanya menghitung 'before' saja.
     * before dihitung terus sampai ketemu ujungnya.
     * @param jalurBulatan
     * @return 
     */
    public int calculateTotalCostByBefore(JalurBulatan jalurBulatan) {
        if (jalurBulatan == null || jalurBulatan.getBefore() == null
                || jalurBulatan.getBefore().getLink() == null) {
            return 0;
        }
        System.out.print(jalurBulatan.getBulatan().getName() + " - "
                + jalurBulatan.getBefore().getLink().getName() + "("
                + jalurBulatan.getBefore().getLink().getCost() + ") - ");
        return jalurBulatan.getBefore().getLink().getCost()
                + calculateTotalCostByBefore(jalurBulatan.getBefore().getBefore());
    }

    /**
     * Rekursif untuk mencari jalur sampai ke finish.
     * @param beforeJalurBulatan 
     */
    protected void calculate(JalurBulatan beforeJalurBulatan) {
        //break untuk rekursif
        if (beforeJalurBulatan == null || beforeJalurBulatan.getBulatan() == null) {
            return;
        }

        List<Link> links = beforeJalurBulatan.getBulatan().getToLinks();
        //break untuk rekursif jika tidak ada link lagi, atau mentok paling pojok
        if (links == null || links.isEmpty()) {
            return;
        }

        //loop mendapatkan percabangan jalur link
        //continue berarti dianggap bukan jalur yang dimaksud, atau tidak mencapai finish
        for (int i = 0; i < links.size(); i++) {
            Link link = links.get(i);

            Bulatan bulatan = link.getToBulatan();
            //jika tidak ada bulatan di continur
            //jika ketemu bulatan yang pernah dilewati sebelumnya, continue
            if (bulatan == null || cekEqualBeforeBulatan(beforeJalurBulatan, bulatan)) {
                continue;
            }

            //buat jalur selanjutnya
            JalurLink jalurLink = new JalurLink();
            jalurLink.setLink(link);
            jalurLink.setBefore(beforeJalurBulatan);

            beforeJalurBulatan.setAfter(jalurLink);

            JalurBulatan jalurBulatan = new JalurBulatan();
            jalurBulatan.setBulatan(bulatan);
            jalurBulatan.setBefore(jalurLink);

            jalurLink.setAfter(jalurBulatan);

            //jika ternyata bulatan ini adalah finish, tambahkan ke daftar jalur start-finish.
            if (bulatan == finishBulatan) {
                this.jalurs.add(jalurBulatan);
                continue;
            }

            //jika bukan finish, lanjutnkan lagi rekursif sampai ketemu finish atau mentok
            calculate(jalurBulatan);
        }
    }

    /**
     * Mengecek apakah bulatan ini pernah dilewati jalur sebelumnya. 
     * TRUE jika pernah dilewati, false jika belum pernah.
     * Kegunaan fungsi ini agar tidak terjadi perputaran jalur tanpa henti.
     * Atau bisa dibilang agar setiap bulatan hanya bisa dilewati satu kali untuk
     * satu jalur
     * 
     * @param jalurBulatan
     * @param bulatan
     * @return 
     */
    protected boolean cekEqualBeforeBulatan(JalurBulatan jalurBulatan, Bulatan bulatan) {
        if (jalurBulatan == null || jalurBulatan.getBefore() == null || bulatan == null) {
            return false;
        }

        if (jalurBulatan.getBulatan() == bulatan) {
            return true;
        }

        return cekEqualBeforeBulatan(jalurBulatan.getBefore().getBefore(), bulatan);
    }

    public Bulatan getStartBulatan() {
        return startBulatan;
    }

    public void setStartBulatan(Bulatan startBulatan) {
        this.startBulatan = startBulatan;
    }

    public Bulatan getFinishBulatan() {
        return finishBulatan;
    }

    public void setFinishBulatan(Bulatan finishBulatan) {
        this.finishBulatan = finishBulatan;
    }

    public List<JalurBulatan> getJalurs() {
        return jalurs;
    }

    public JalurBulatan getLowCostJalurBulatan() {
        return lowCostJalurBulatan;
    }
}
